"""
@Time : 2023/4/17 15:33 
@项目：JYairports
@File : encry_decry_algorithm.by
@PRODUCT_NAME :PyCharm
"""
import hashlib
import base64
import hmac

"""
MD5
MD5的全称是Message-Digest Algorithm 5（信息-摘要算法）。128位长度。目前MD5是一种不可逆算法。
具有很高的安全性。它对应任何字符串都可以加密成一段唯一的固定长度的代码。
SHA1
SHA1的全称是Secure Hash Algorithm(安全哈希算法) 。SHA1基于MD5，加密后的数据长度更长，
它对长度小于264的输入，产生长度为160bit的散列值。比MD5多32位。
因此，比MD5更加安全，但SHA1的运算速度就比MD5要慢了。
"""


class EncryDecryAlgorithm(object):
    @staticmethod
    def sha256(text):  # 可用于修改密码
        return hashlib.sha256(text.encode("utf-8")).hexdigest()

    @staticmethod
    def sha512(text):
        return hashlib.sha512(text.encode("utf-8")).hexdigest()

    en_str = base64.b64encode(b'123456Aa')
    de_str = base64.b64decode(b'MTIzNDU2QWE=').decode('ascii')

    @staticmethod
    def b64encode(text):
        return base64.b64encode(text.encode('utf-8'))

    @staticmethod
    def b64decode(text):
        if isinstance(text, bytes):
            base64.b64decode(text).decode('ascii')
        else:
            base64.b64decode(text.encode('utf-8')).decode('ascii')

    # <editor-fold desc="md5加密">
    @staticmethod
    def md5_encrypt(encrypt_str, encoding='utf-8'):
        """
        :param encrypt_str: 加密的字符串
        :return: 长度为32位的16进制字符串
        """
        md = hashlib.md5()
        md.update(encrypt_str.encode(
            encoding=encoding  #
        ))
        return md.hexdigest()

    # </editor-fold>
    # <editor-fold desc="md5解密">
    @staticmethod
    def md5_decryption(md5_encrypt_str, encrypt_str, encoding='utf-8'):
        md = hashlib.md5()
        md.update(encrypt_str.encode(
            encoding=encoding  #
        ))
        if md5_encrypt_str == md.hexdigest():
            return True
        else:
            return False

    # </editor-fold>

    # <editor-fold desc="sha1-加密-安全哈希算法">
    @staticmethod
    def sha1_encrypt(encrypt_str, encoding='utf-8'):
        """
        :param encrypt_str: 加密的字符串
        :return: 长度为40位的16进制字符串
        """
        sha = hashlib.sha1(encrypt_str.encode(encoding))
        encrypts = sha.hexdigest()
        return encrypts

    # </editor-fold>
    # <editor-fold desc="sha1-解密-安全哈希算法">
    @staticmethod
    def sha1_decryption(sha1_encrypt_str, encrypt_str, encoding='utf-8'):
        """
        :param encrypt_str: 加密的字符串
        :return: 长度为40位的16进制字符串
        """
        sha = hashlib.sha1(encrypt_str.encode(encoding))
        encrypts = sha.hexdigest()
        if sha1_encrypt_str == encrypts:
            return True
        else:
            return False

    # </editor-fold>
    # <editor-fold desc="HMAC加密">
    """HMAC加密
    全称：散列消息鉴别码(Hash Message Authentication Code)， HMAC加密算法是一种安全的基于加密hash函数和共享密钥的消息认证协议。
    实现原理是用公开函数和密钥产生一个固定长度的值作为认证标识，用这个标识鉴别消息的完整性。
    使用一个密钥生成一个固定大小的小数据块，即 MAC，并将其加入到消息中，然后传输。
    接收方利用与发送方共享的密钥进行鉴别认证等。Python代码："""

    @staticmethod
    def mac_encrypt(key, encrypt_str):
        # 第一个参数是密钥key，第二个参数是待加密的字符串，第三个参数是hash函数
        # mac = hmac.new('key', 'hello', hashlib.md5)
        mac = hmac.new(key, encrypt_str, hashlib.md5)
        mac.digest()  # 字符串的ascii格式
        mac.hexdigest()  # 加密后字符串的十六进制格式
    # </editor-fold>


# LXa539## / NGq247##
#
# 6. SwaggerAPI账号密码为 LXa539## / 8a41bb97b62b881efb891f302794c921fa502402e48e6d1aefbdcb92c35e7113
# print(hashlib.sha256("NGq247##".encode("utf-8")).hexdigest())


def decrypt_aes(sSrc, key, iv):
    """
    AES 解密
    :param sSrc:
    :param key:
    :param iv:
    :return:
    """
    try:
        raw = key.encode('ASCII')
        skey_spec = AES.new(raw, AES.MODE_CBC, iv.encode())
        encrypted = base64.b64decode(sSrc)
        original = skey_spec.decrypt(encrypted)
        return original.decode("utf-8")
    except Exception as e:
        print(e)
        raise e
