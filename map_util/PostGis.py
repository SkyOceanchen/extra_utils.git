"""
    关联postgis的一系列操作
"""
from utils.other_utils.Connect import TKConnect
import psycopg2
import json
from utils.map_utils.GeoTrans import TKGeoTrans
from airportdata.models import Airport, MapLayer


class TKPostGis:
    """
        TK arcgis 相关的数据库操作
    """

    def __init__(self, dbsettings):
        self.conn = psycopg2.connect(database=dbsettings['NAME'], user=dbsettings['USER'],
                                     password=dbsettings['PASSWORD'], host=dbsettings['HOST'], port=dbsettings['PORT'])
        self.airport_id = 1
        cur_airport = Airport.objects.get(id=self.airport_id)
        mapdata = eval(cur_airport.gis_trans_attr)
        self.wkid = mapdata.get('wkid')
        self.closearea_name = "土面区"

    @staticmethod
    def dictfetchall(cursor):
        """
            以字典格式返回数据
        :param cursor:
        :return:
        """
        desc = cursor.description
        return [
            dict(zip([col[0] for col in desc], row))
            for row in cursor.fetchall()
        ]

    def query(self, sql, params: list):
        """
            手动查询sql
        :param sql:
        :param params:
        :return:
        """
        cursor = self.conn.cursor()
        cursor.execute(sql, params)
        result = self.dictfetchall(cursor)
        self.conn.commit()
        self.conn.close()
        return result

    def difference(self, wkt1, wkt2):
        """
            从A去除和B相交的部分
        :param wkt1:
        :param wkt2:
        :return:
        """
        sql = f"""SELECT st_astext(st_difference(st_geometry('{wkt1}',{self.wkid}),st_geometry('{wkt2}',{self.wkid})));"""
        cursor = self.conn.cursor()
        cursor.execute(sql)
        res = cursor.fetchall()
        return res[0][0]

    def intersection(self, wkt1, wkt2):
        """
            取两个要素的交集
        :param wkt1:
        :param wkt2:
        :return:
        """
        sql = f"""SELECT st_astext(st_intersection(st_geometry('{wkt1}',{self.wkid}),st_geometry('{wkt2}',{self.wkid})));"""
        cursor = self.conn.cursor()
        cursor.execute(sql)
        res = cursor.fetchall()
        return res[0][0]

    def union(self, wkt1, wkt2):
        """
            取两个要素的并集
        :param wkt1:
        :param wkt2:
        :return:
        """
        sql = f"""SELECT st_astext(st_union(st_geometry('{wkt1}',{self.wkid}),st_geometry('{wkt2}',{self.wkid})));"""
        cursor = self.conn.cursor()
        cursor.execute(sql)
        res = cursor.fetchall()
        return res[0][0]

    def extent(self, wkt):
        sql = f"""SELECT st_astext(st_envelope(st_geometry('{wkt}',{self.wkid})));"""
        cursor = self.conn.cursor()
        cursor.execute(sql)
        res = cursor.fetchall()
        extent_wkt = res[0][0]
        extent = TKGeoTrans().wkt_get_bound(extent_wkt)
        return list(extent)

    def shape(self, obj_id):
        sql = f"""SELECT st_astext(shape) from "土面区" where objectid={obj_id};"""
        cursor = self.conn.cursor()
        cursor.execute(sql)
        res = cursor.fetchall()
        return res[0][0]

    def close(self):
        self.conn.commit()
        self.conn.close()

    def query1(self):
        """
            土面区影响区geojson经过的土面区objectid列表
        :param sql:
        :param params:
        :return:
        """
        [query_field, tab] = self.search_for_tab_queryfield(self.closearea_name)
        sql = f"""SELECT {query_field}, st_astext(shape) as shape from {tab};"""
        cursor = self.conn.cursor()
        cursor.execute(sql)
        res = cursor.fetchall()
        return res

    def search_for_tab_queryfield(self, layer_name):
        layer = MapLayer.objects.get(airport__id=self.airport_id, layer_name=layer_name)
        return [layer.query_field, layer.query_table] if layer else [None, None]

# if __name__ == '__main__':
#     TKPostGis({
#         'ENGINE': 'django.db.backends.postgresql_psycopg2',
#         'NAME': 'pvg',
#         'USER': 'postgres',
#         'PASSWORD': 'root',
#         'HOST': '47.110.58.23',
#         'PORT': '15432',
#     }).cal_cover(1,2)
