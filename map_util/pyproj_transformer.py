# -*- coding: utf-8 -*-
"""
@Time : 2024/5/21 17:07 
@项目：xianPRo
@File : pyproj_transformer.by
@PRODUCT_NAME :PyCharm
"""

import pyproj
# 定义大地坐标系
crs = pyproj.CRS.from_string('+proj=tmerc +ellps=WGS84 +datum=WGS84 +units=m +no_defs')
# 定义经纬度坐标系
crs_latlon = pyproj.CRS.from_string('EPSG:4326')
# 创建坐标转换器
transformer = pyproj.Transformer.from_crs(crs, crs_latlon, always_xy=True)
# 大地坐标系转换为经纬度
x, y = 500000, 4000000
lon, lat = transformer.transform(x, y)
print(f'经度: {lon}, 纬度: {lat}')