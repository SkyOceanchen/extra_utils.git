# -*- coding:utf-8 -*-
"""
@Time : 2023/3/26
@Author : skyoceanchen

@
@File : JYmap_operations.py 
@PRODUCT_NAME : PyCharm 
"""

from math import sin, radians, fabs, cos, asin, sqrt
import math


class Map(object):
    # <editor-fold desc="计算两个经纬度 之间的距离">
    @staticmethod
    def get_distance_hav(lat0, lng0, lat1, lng1):
        EARTH_RADIUS = 6371.137  # 地球平均半径大约6371km

        def hav(theta):
            s = sin(theta / 2)
            return s * s

        # 用haversine公式计算球面两点间的距离
        # 经纬度转换成弧度
        lat0 = radians(lat0)
        lat1 = radians(lat1)
        lng0 = radians(lng0)
        lng1 = radians(lng1)
        dlng = fabs(lng0 - lng1)
        dlat = fabs(lat0 - lat1)
        h = hav(dlat) + cos(lat0) * cos(lat1) * hav(dlng)
        distance = 2 * EARTH_RADIUS * asin(sqrt(h))  # km
        return distance

    # </editor-fold>
    # <editor-fold desc="经纬度转大地坐标">
    @staticmethod
    def transfRefer(LONG, LAT):
        x0 = LONG * 20037508.34 / 180
        y0 = math.log(math.tan((90 + LAT) * math.pi / 360)) / (math.pi / 180)
        y0 = y0 * 20037508.34 / 180
        return x0, y0
    # </editor-fold>


print(Map().transfRefer(
    108.78158677949999, 34.43860414600001
))