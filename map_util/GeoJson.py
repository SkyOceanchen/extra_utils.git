def Point(x, y):
    return {"type": 'point', "x": float(x), "y": float(y)}


def Feature(geometry, attributes={}):
    return {"attributes": attributes, "geometry": geometry}


def FeatureSet(ALLFeature, wkid):
    # FID 必要
    Feature = ALLFeature[0]
    fields = []
    attributes = Feature.get("attributes")
    fid = attributes.get("FID", '')
    if fid == '':
        fields.append({"name": "FID", "type": "esriFieldTypeOID", "alias": "FID"})
    for key in attributes:
        if key == "FID":
            fields.append({"name": "FID", "type": "integer", "alias": "FID"})
        else:
            fields.append({"name": key, "type": "string", "alias": key, "length": 254})
    return {
        "displayFieldName": "",
        "fieldAliases": {"FID": "FID"},
        "geometryType": "point",
        "spatialReference": {
            "wkid": wkid
        },
        "fields": fields,
        "features": ALLFeature
    }


def LayerDefinition(ALLFeature):
    Feature = ALLFeature[0]
    fields = []
    attributes = Feature.get("attributes")
    fid = attributes.get("FID", '')
    if fid == '':
        fields.append({"name": "FID", "type": "esriFieldTypeOID", "alias": "FID"})
    for key in attributes:
        if key == "FID":
            fields.append({"name": "FID", "type": "esriFieldTypeOID", "alias": "FID"})
        else:
            fields.append({"name": key, "type": "esriFieldTypeString", "alias": key, "length": 254})
    return {
        "geometryType": "esriGeometryPoint",
        "fields": fields
    }
