# -*- coding:utf-8 -*-
"""
@Time : 2022/3/20
@Author : skyoceanchen

@
@File : area_location.py 
@PRODUCT_NAME : PyCharm 
"""
y1 = 9647805
y2 = 9647865
x1 = 3996592
x2 = x1 + 1500
x3 = x2 + 1500
x4 = x3 + 1500


def if_areas(x, y, ):
    if y1 < y < y2:
        if x1 <= x < x2:
            return "locate in surface one"
        elif x2 <= x < x3:
            return "locate in surface 2"
        elif x3 <= x <= x4:
            return "locate in surface 3"
        else:
            return "x out of range"
    else:
        return "y out of range"


import math
import decimal
from decimal import Context, ROUND_HALF_UP, ROUND_FLOOR


class AreaLocation(object):
    def __init__(self):
        self.u = ''
        self.a = 6378137
        self.b = 6356752.314
        self.rou = 57.29577951
        self.ee = 0.082094438
        self.e = 0.081819191
        self.A0 = 1.005052502
        self.A2 = -0.002531554
        self.A4 = 2.6569E-06
        self.A6 = -3.46953E-09
        self.A8 = 4.82661E-12

        self.x0 = -683172.049743176
        self.y0 = -3457833.585495
        self.m = 0.999980184982068  # '比例因子'
        self.B = -1.2217021252754  # '旋转角度 弧度（ 文佳佳）'
        self.L0 = 120  # '本初子午线'

        self.y1 = 9647805.71174143
        self.y2 = 9647805.71174160
        self.x1 = 3996592.881009059
        self.x2 = self.x1 + 1500
        self.x3 = self.x2 + 1500
        self.x4 = self.x3 + 1500

    def l(self, lon):
        return (lon - self.L0) / self.rou

    def t(self, lat):
        return math.tan(lat / self.rou)

    def inta2(self, lat):
        return math.pow(self.ee, 2) * math.pow((math.cos((lat / self.rou))), 2)

    def N(self, lat):
        return self.a / math.sqrt((1 - self.e * self.e * math.pow(math.sin(lat / self.rou), 2)))

    def X(self, lat):
        return self.a * (1.0 - math.pow(self.e, 2)) * \
               (self.A0 * lat / self.rou + self.A2 * math.sin(2 * lat / self.rou) + self.A4
                * math.sin(4.0 * lat / self.rou) + self.A6
                * math.sin(6.0 * lat / self.rou) + self.A8
                * math.sin(8.0 * lat / self.rou))

    def y(self, lon, lat):
        return ((self.l(lon)
                 * self.N(lat)
                 * math.cos(lat / self.rou)
                 + math.pow(self.l(lon), 3)
                 / 6.0
                 * self.N(lat)
                 * (math.pow(math.cos(lat / self.rou), 3))
                 * (1 - math.pow(self.t(lat), 2) + self.inta2(lat))
                 + math.pow(self.l(lon), 5)
                 / 120.0
                 * self.N(lat)
                 * math.pow((math.cos(lat / self.rou)), 5)
                 * (5.0 - 18.0 * math.pow(self.t(lat), 2) + math.pow(self.t(lat), 4)
                    + 14.0 * self.inta2(lat) - 58.0 * self.inta2(lat)
                    * math.pow(self.t(lat), 2)))) + 500000

    def x(self, lon, lat):
        return self.X(lat) \
               + self.N(lat) \
               * self.t(lat) \
               * math.pow(math.cos(lat / self.rou), 2) \
               * math.pow(self.l(lon), 2) \
               * (1.0 / 2.0 + 1.0
                  / 24.0
                  * (5.0 - math.pow(self.t(lat), 2) + 9.0
                     * self.inta2(lat) + 4.0 * math.pow(self.inta2(lat), 2))
                  * math.pow(math.cos(lat / self.rou), 2)
                  * (math.pow(self.l(lon), 2))) + 1.0 / 720.0 \
               * (61.0 - 58.0 * math.pow(self.t(lat), 2) + math.pow(self.t(lat), 4)) \
               * math.pow(math.cos(lat / self.rou), 4) * math.pow(self.l(lon), 4)

    def P0(self, lon, lat):
        return self.y0 + self.m * self.y(lon, lat) - self.x(lon, lat) * self.B

    def H0(self, lon, lat):
        return self.x0 + self.m * self.x(lon, lat) + self.B * self.y(lon, lat)

    def lonLat2XY(self, lon, lat):
        # l = self.l(lon)
        # l = round(l, 9)
        #
        # t = self.t(lat)
        # t = round(t, 9)
        #
        # inta2 = self.inta2(lat)
        # inta2 = round(inta2, 9)
        #
        # N = self.N(lat)
        # N = round(N, 9)
        #
        # X = self.X(lat)
        # X = round(X, 9)
        y = self.y(lon, lat)
        y = float(decimal.Decimal(y).quantize(decimal.Decimal('.000000000'), rounding=ROUND_HALF_UP))
        x = self.x(lon, lat)
        x = float(decimal.Decimal(x).quantize(decimal.Decimal('.000000000'), rounding=ROUND_HALF_UP))
        NEW_X = self.y0 + y * self.m * math.cos(self.B) - x * self.m * math.sin(self.B)

        NEW_Y = self.x0 + y * self.m * math.sin(self.B) + x * self.m * math.cos(self.B)
        result = [NEW_X + self.x0
            , NEW_Y + self.y0]
        return result


if __name__ == '__main__':
    print(AreaLocation().lonLat2XY(108.78158677949999,34.43860414600001, ))
    # import time
    #
    # t1 = time.time()
    # LonLat = AreaLocation()
    # # while True:
    # #     print('*' * 100)
    # #     init_y = int(input("input coordinate y: "))
    # #     init_x = int(input("input coordinate x: "))
    # #     x, y = LonLat.lonLat2XY(init_x, init_y)
    # #     print("转换后坐标x：{},y:{}".format(str(x), str(y)))
    # #     result = if_areas(x, y)
    # #     print(result)
    # d = [{'latitude': 31.26960182, 'longitude': 121.21048737}, {'latitude': 31.27447065, 'longitude': 121.20528158},
    #      {'latitude': 31.27933947, 'longitude': 121.20007579}, {'latitude': 31.2842083, 'longitude': 121.19487}]
    # for i in d:
    #     init_x = i.get("latitude")
    #     init_y = i.get("longitude")
    #     x, y = LonLat.lonLat2XY(init_x, init_y)
    #     print(x, y)
    #     print(time.time() - t1)
    #     #     print("转换后坐标x：{},y:{}".format(str(x), str(y)))
