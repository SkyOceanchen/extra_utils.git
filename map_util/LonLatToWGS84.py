import math

# <editor-fold desc="坐标转换基础参数">
a = 6378137
e = 0.081819191
ee = 0.082094438
rou = 57.29577951
A0 = 1.005052502
A2 = -0.002531554
A4 = 2.6569E-06
A6 = -3.46953E-09
A8 = 4.82661E-12


# </editor-fold>


class LonLatToWGS84:

    def __init__(self, gis_trans_attr):
        self.x0 = gis_trans_attr['y']
        self.L0 = gis_trans_attr['L0']
        self.y0 = gis_trans_attr['x']
        self.A = gis_trans_attr['scale']
        self.B = gis_trans_attr['range']
        self.Ax = gis_trans_attr['Ax']
        self.Ay = gis_trans_attr['Ay']

    def l(self, lon):
        return (lon - self.L0) / rou

    def t(self, lat):
        return math.tan((lat / rou))

    def inta2(self, lat):
        return math.pow(ee, 2) * math.pow((math.cos((lat / rou))), 2)

    def N(self, lat):
        return a / math.sqrt((1 - e * e * math.pow(math.sin(lat / rou), 2)))

    def X(self, lat):
        return a * (1.0 - math.pow(e, 2)) * (
                A0 * lat / rou + A2 * math.sin(2 * lat / rou) + A4 * math.sin(4.0 * lat / rou) + A6 * math.sin(
            6.0 * lat / rou) + A8 * math.sin(8.0 * lat / rou))

    def x(self, lon, lat):
        return self.X(lat) + self.N(lat) * self.t(lat) * math.pow(math.cos(lat / rou), 2) * math.pow(self.l(lon), 2) * (
                1.0 / 2.0 + 1.0 / 24.0 * (
                5.0 - math.pow(self.t(lat), 2) + 9.0 * self.inta2(lat) + 4.0 * math.pow(self.inta2(lat),
                                                                                        2)) * math.pow(
            math.cos(lat / rou), 2) * (math.pow(self.l(lon), 2))) + 1.0 / 720.0 * (
                       61.0 - 58.0 * math.pow(self.t(lat), 2) + math.pow(self.t(lat), 4)) * math.pow(
            math.cos(lat / rou), 4) * math.pow(self.l(lon), 4)

    def y(self, lon, lat):
        return self.l(lon) * self.N(lat) * math.cos(lat / rou) + math.pow(self.l(lon), 3) / 6.0 * self.N(lat) * (
            math.pow(math.cos(lat / rou), 3)) * (1 - math.pow(self.t(lat), 2) + self.inta2(lat)) + math.pow(self.l(lon),
                                                                                                            5) / 120.0 * self.N(
            lat) * math.pow((math.cos(lat / rou)), 5) * (
                       5.0 - 18.0 * math.pow(self.t(lat), 2) + math.pow(self.t(lat), 4) + 14.0 * self.inta2(
                   lat) - 58.0 * self.inta2(lat) * math.pow(self.t(lat), 2))

    def P0(self, lon, lat):
        return self.y0 + self.A * self.y(lon, lat) - self.x(lon, lat) * self.B

    def H0(self, lon, lat):
        return self.x0 + self.A * self.x(lon, lat) + self.B * self.y(lon, lat)

    def LonLat(self, lon, lat):
        NEW_X = self.P0(lon, lat)
        NEW_Y = self.H0(lon, lat)
        return NEW_X + self.Ax, NEW_Y + self.Ay


if __name__ == "__main__":
    Lon =   108.78158677949999
    Lat = 34.43860414600001
    gis_trans_attr = {"y": -1271784.748, "x": 3215177.986, "scale": 0.324257922, "range": 0.945560463, "L0": 120,
                      "Ax": 0, "Ay": 0, "xmin": 7257.0067999996245,
                      "ymin": 7098.49980000034, "xmax": 15396.506799999624, "ymax": 12480.075800001621, "maxScale": 250,
                      "minScale": 64000
                      }

    # <editor-fold desc="使用 结果= 10628.16913100984, 7750.605025201279">
    LonLat = LonLatToWGS84(gis_trans_attr)
    position = LonLat.LonLat(Lon, Lat)
    print("position",position)
    # </editor-fold>
