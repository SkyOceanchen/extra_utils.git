import math
import scipy.stats
from scipy.optimize import curve_fit
import numpy as np
from collections import Counter


# import matplotlib.pyplot as plt

class MathOperations(object):
    # <editor-fold desc="# 计算平均值">
    def mean(self, lis: list):
        return sum(lis) / len(lis)

    # </editor-fold>
    # <editor-fold desc="众数">
    def mode(self, lis: list):
        counts = Counter(lis)
        max_count = max(counts.values())
        return [x_i for x_i, count in counts.items()
                if count == max_count]

    # </editor-fold>
    """中位数的一个泛化概念是分位数（quantile），它表示少于数据中特定百分比的一个值。（中位数表示少于50% 的数据的一个值。"""

    # <editor-fold desc="求中位数">
    def median(self, lis: list, ):
        """finds the 'middle-most' value of v"""
        n = len(lis)
        sorted_v = sorted(lis)
        midpoint = n // 2
        if n % 2 == 1:
            # 如果是奇数，返回中间值
            return sorted_v[midpoint]
        else:
            # 如果是偶数，返回中间两个值的均值
            lo = midpoint - 1
            hi = midpoint
            return (sorted_v[lo] + sorted_v[hi]) / 2

    # </editor-fold>
    # <editor-fold desc="分位数">
    def quantile(self, lis: list, p=0.5):
        """returns the pth-percentile value in x"""
        p_index = int(p * len(lis))
        return sorted(lis)[p_index]

    # </editor-fold>
    # <editor-fold desc="分位数之差">
    """
    极差和标准差也都有我们之前提到的均值计算常遇到的异常值问题。再看之前的例子，如
    果我们最具人缘的用户有200 个朋友，标准差就变为14.89，增加了60% ！
    一种更加稳健的方案是计算75% 的分位数和25% 的分位数之差
    """

    def interquartile_range(self, x):
        return self.quantile(x, 0.75) - self.quantile(x, 0.25)

    # </editor-fold>
    # <editor-fold desc="最小公倍数">
    def lcm(self, x, y):
        """
                if x > y:
            greater = x
        else:
            greater = y
        while True:
            if ((greater % x == 0) and (greater % y == 0)):
                num = greater
                break
            greater += 1
        return num
        """
        return int(x * y / math.gcd(x, y))

    # </editor-fold>
    # <editor-fold desc="最大公约数">
    def gcd(self, x, y):
        """
                if x > y:
            smaller = y
        else:
            smaller = x
        for i in range(1, smaller + 1):
            if ((x % i == 0) and (y % i == 0)):
                num = i
        return num
        """
        return math.gcd(x, y)

    # </editor-fold>
    # <editor-fold desc="# 方差">
    def variance(self, x):
        n = len(x)
        deviations = self.de_mean(x)
        return self.sum_of_squares(deviations) / (n - 1)

    # </editor-fold>
    # <editor-fold desc="# 标准差">
    def standard_deviation(self, x):
        return math.sqrt(self.variance(x))

    # </editor-fold>
    # <editor-fold desc="# 协方差">
    def covariance(self, x, y):
        """
        协方差（covariance），这个概念是方差的一个对应词。方差衡量了单个变
        量对均值的偏离程度，而协方差衡量了两个变量对均值的串联偏离程度：
        """
        n = len(x)
        return self.dot(self.de_mean(x), self.de_mean(y)) / (n - 1)

    # </editor-fold>
    """离散度是数据的离散程度的一种度量。通常，如果它所统计的值接近零，则表示数据聚集在一起，离散程度很小，如果值很大（无论那意味着什么），则表示数据的离散度很大。"""

    # <editor-fold desc="点乘-向量相乘">
    def dot(self, v, w):
        """
        # 点乘（dot product）。两个向量的点乘表示对应元素的分量乘积之和：
        v_1 * w_1 + ... + v_n * w_n
        """
        return sum(v_i * w_i for v_i, w_i in zip(v, w))

    # </editor-fold>
    # <editor-fold desc="平方和">
    def sum_of_squares(self, v):
        """
          通过点乘很容易计算一个向量的平方和：
          v_1 * v_1 + ... + v_n * v_n
          """
        return self.dot(v, v)

    # </editor-fold>
    # <editor-fold desc=" # 计算每一项数据与均值的差">
    def de_mean(self, x):
        """translate x by subtracting its mean (so the result has mean 0)"""
        x_bar = self.mean(x)
        return [x_i - x_bar for x_i in x]

    # </editor-fold>
    # <editor-fold desc="极差-一个简单的度量">
    def data_range(self, x):
        """
        一个简单的度量是 极差（range），指最大元素与最小元素的差
        :param x:
        :return:
        """
        return max(x) - min(x)

    # </editor-fold>
    # <editor-fold desc="幂函数">
    def func_power(self, x, a, b):
        return x ** a + b

    # </editor-fold>
    # <editor-fold desc="指数函数">
    def exp_arithm(self, x, a, b):
        """
          print(math.exp(2))#高等数学里以自然常数e为底的指数函数,返回e的n次方
        """
        return a ** x + b

    # </editor-fold>
    # <editor-fold desc="对数函数">
    def log_arithm(self, num, blacknum=2):
        """
        x=logaN
        :param num: 取对数
        :param blacknum:底数
        :return:
        """
        # print(e)  # 自然对数
        # print(log(e))  # log函数默认是以e为底
        # print(log(100, 10))  # 以10为底，对100取对数
        # print(log(2, e))  # 以e为底，对2取对数
        return math.log(num, blacknum)

    # </editor-fold>


class LinearFittingOperations(object):
    # <editor-fold desc="# 线性 相关系数">
    def correlation(self, x, y):
        """
            协方差除以两个变量的标准差
            :param x: 列表1
            :param y: 列表2
            :return:相关系数没有单位，它的取值在-1（完全反相关）和1（完全相关）之间。相关值0.25 代表一个相对较弱的正相关。
        """
        math_obj = MathOperations()
        stdev_x = math_obj.standard_deviation(x)
        stdev_y = math_obj.standard_deviation(y)
        if stdev_x > 0 and stdev_y > 0:
            return pow(math_obj.covariance(x, y) / stdev_x / stdev_y, 2)
        else:
            return 0  # 如果没有变动，相关系数为零

    # </editor-fold>
    # <editor-fold desc="有异常值时的相关系数">
    def abnormal_correlation(self, x, y):
        outlier = x.index(len(x))  # outlier的索引
        num_friends_good = [x for i, x in enumerate(x) if i != outlier]
        daily_minutes_good = [x for i, x in enumerate(y) if i != outlier]
        return self.correlation(num_friends_good, daily_minutes_good)

    # </editor-fold>
    # <editor-fold desc="线性拟合">
    def linefit(self, x, y):
        """
            X = [1, 2, 3, 4, 5, 6]
        Y = [2.5, 3.51, 4.45, 5.52, 6.47, 7.51]
        a, b, r = linefit(X, Y)
        print("X=", X)
        print("Y=", Y)
        print("拟合结果: y = %10.5f x + %10.5f , r=%10.5f" % (a, b, r))
    # %f 表示以浮点数形式对数值进行输出，
    # 10.5代表整个输出数值会占10个字符位置，
    # 如果不足，左边会自动补上空格，同时在这个10个字符位置中，
    # 小数点后会保留5位，如原值 12.235256，则会显示_ _ _12.23456
        :param x:
        :param y:
        :return:
        """
        N = float(len(x))
        sx, sy, sxx, syy, sxy = 0, 0, 0, 0, 0
        for i in range(0, int(N)):
            sx += x[i]
            sy += y[i]
            sxx += x[i] * x[i]
            syy += y[i] * y[i]
            sxy += x[i] * y[i]
        a = (sy * sx / N - sxy) / (sx * sx / N - sxx)
        b = (sy - a * sx) / N
        r = abs(sy * sx / N - sxy) / math.sqrt((sxx - sx * sx / N) * (syy - sy * sy / N))
        return a, b, r

    # </editor-fold>
    # <editor-fold desc="# 求线性相关的a，b  y = a*x+b">
    def slope_intercept(self, x, y):
        result = scipy.stats.linregress(x, y)
        slope = result.slope  # 回归线的斜率
        intercept = result.intercept  # 回归线的截距
        rvalue = pow(result.rvalue, 2)  # 相关系数
        # print(result.pvalue)  # p值
        # print(result.stderr)  # 估计梯度的标准误差
        return slope, intercept, rvalue

    # </editor-fold>
    # <editor-fold desc="线性回归分析，线性拟合">
    # https://blog.csdn.net/L_goodboy/article/details/123900297
    # 获取线性拟合方程式a,b
    def line_fitting_fun(self, x_array, y_array):
        # 方程个数
        m = len(x_array)
        # 计算过程
        sum_x = np.sum(x_array)
        sum_y = np.sum(y_array)
        sum_xy = np.sum(x_array * y_array)
        sum_xx = np.sum(x_array ** 2)
        a = (sum_y * sum_xx - sum_x * sum_xy) / (m * sum_xx - (sum_x) ** 2)
        b = (m * sum_xy - sum_x * sum_y) / (m * sum_xx - (sum_x) ** 2)
        # print("p = {:.4f} + {:.4f}x".format(a, b))
        return a, b

    # 线性回归分析，线性拟合

    def line_fitting(self, x, y, ):
        """
        使用回归分析绘制拟合曲线是一种常见的方法，简单线性回归就是其中的一种。
        简单线性回归可以通过最小二乘法来计算回归系数。
        以下是一个使用简单线性回归来拟合数据的代码示例：
           x = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
        y = np.array([2.5, 4.5, 4.8, 5.5, 6.0, 7.0, 7.8, 8.0, 9.0, 9.5])
        """
        x = np.array(x)
        y = np.array(y)
        # 计算回归系数
        slope, intercept = np.polyfit(x, y, 1)
        # 绘制拟合曲线
        # plt.scatter(x, y)
        y = slope * x + intercept
        # plt.plot(x, y, color='red')
        # plt.show()
        # print()
        equation = " {:.4f}x + {:.4f}".format(slope, intercept, )
        return x, y, equation

    # </editor-fold>
    # <editor-fold desc="多项式线性拟合">

    def many_line_fitting(self, x, y, frequency=2):
        """
        frequency =2 是2项式   frequency =3 是3项式
        多项式回归是一种常用方法，它可以用来拟合更加复杂的数据集,
        与简单线性回归不同，多项式回归可以拟合更加复杂的数据集。
        ，np.polyfit函数计算多项式回归系数，np.poly1d函数生成一个多项式拟合对象
               x = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
        y = np.array([2.5, 4.5, 4.8, 5.5, 15.0, 7.0, 10.8, 8.0, 9.0, 9.5])
        """
        x = np.array(x)
        y = np.array(y)

        # 计算多项式回归系数
        coefs = np.polyfit(x, y, frequency)
        # 使用np.poly1d函数来生成一个多项式拟合对象
        poly = np.poly1d(coefs)
        # 生成新的横坐标，使得拟合曲线更加平滑
        foot = 1000
        new_x = np.linspace(min(x), max(x), len(x) * foot)
        # print(len(new_x[::foot]), len(poly(new_x)[::foot]))
        # 绘制拟合曲线
        # plt.scatter(x, y)
        # plt.plot(new_x[::foot], poly(new_x)[::foot], color='red')
        # plt.show()
        return new_x[::foot], poly(new_x)[::foot]

    # </editor-fold>
    # <editor-fold desc="非线性拟合，需要选择算法模型">
    # 算法模型
    def func(self, x, a, b, c):
        return a * np.exp(-b * x) + c
        # return  1.0 / (1.0 + np.exp(-a * (x-b))) + c

    def nit_line_fitting(self, x=None, y=None):
        # 生成模拟数据
        x_data = np.linspace(0, 4, 50)
        y_data = LinearFittingOperations.func(x_data, 2.5, 1.3, 0.5) + 0.2 * np.random.normal(size=len(x_data))
        LinearFittingOperations.many_line_fitting(x_data.tolist(), y_data.tolist())
        # x_data = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
        # y_data = np.array([2.5, 4.5, 4.8, 5.5, 6.0, 7.0, 7.8, 8.0, 9.0, 9.5])
        # 使用curve_fit函数来拟合非线性数据
        popt, pcov = curve_fit(LinearFittingOperations.func, x_data, y_data)

        # 画出原始数据和拟合曲线
        # plt.scatter(x_data, y_data, label="Data111111111")
        # plt.plot(x_data, func(x_data, *popt), color='red', label="Fitted curve")
        # plt.legend()
        # plt.show()
    # </editor-fold>

#
# if __name__ == '__main__':
#     x = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
#     y = [2.5, 4.5, 4.8, 5.5, 6.0, 7.0, 7.8, 8.0, 9.0, 9.5]
#     print(LinearFittingOperations.line_fitting(x, y))
