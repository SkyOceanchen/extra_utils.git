from pavement.models import Damage
from basic.models import Area, Config
import datetime as dt
from pavement.conf import *


class DamagePriority:
    # smooth  平滑道
    # liaison  联络道
    # quick  快滑道
    # apron  机坪
    # runway  跑道
    part_class_weight = {
        'runway': 1,
        'apron': 0.5,
        'smooth': 0.8,
        'liaison': 0.8,
        'quick': 0.8,
    }
    threat_weight = {
        1: 0.4,
        2: 0.8,
        3: 1
    }
    name = 'pavement'

    def priority(self, damage: Damage):
        threat_level = 1
        part_class = None
        area_code = damage.domain
        if Area.objects.filter(area_code=area_code):
            this_area = Area.objects.get(area_code=area_code)
            part_class = this_area.part.part_class
            threat_level = getattr(damage.damage_species, part_class)
        overtime_days = (dt.datetime.now() - damage.cur_node_time).days
        overtime_hours = (dt.datetime.now() - damage.cur_node_time).seconds / 3600 + overtime_days * 24
        conf_field = OVERTIME.get(damage.status, None)
        days = 0
        if conf_field:
            overhours = Config.objects.get(app_label=self.name, conf_field=conf_field).conf_value
            days = (int(overhours) - overtime_hours) / 24
        part_score = 0.5 * self.part_class_weight.get(part_class, 0)
        threat_score = 0.3 * self.threat_weight.get(threat_level, 0.2)
        if days > 5:
            over_score = 0.2 * 0.4
        elif days > 3:
            over_score = 0.2 * 0.6
        elif days > 1:
            over_score = 0.2 * 0.8
        else:
            over_score = 0.2 * 1
        score = part_score + threat_score + over_score
        priority = 1
        if score > 0.3:
            priority = 1
        elif score > 0.6:
            priority = 2
        else:
            priority = 3
        return priority

# 位置总权重	0.5	威胁等级总权重	0.3	提醒时间总权重	0.2	相同得分按时间先后排序
# 跑道	1	高威胁	1	超时	1
# 滑行道	0.8	中威胁	0.8	1-3天	0.8
# 机坪	0.5	道面低威胁	0.4	3-5天	0.6
# 	0	其他设施	0.2	大于5天	0.4
#
# 案例	跑道断板			3天		得分
# 	0.5*1=0.5	0.3*1=0.3		0.2*0.8=0.16		0.5+0.3+0.16=0.96
