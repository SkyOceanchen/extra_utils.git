"""数据库连接外部非项目数据库"""

from django.db import connections


class TKConnect:

    def __init__(self, db_alias):
        self.conn = connections[db_alias]

    @staticmethod
    def dictfetchall(cursor):
        """
            以字典格式返回数据
        :param cursor:
        :return:
        """
        desc = cursor.description
        return [
            dict(zip([col[0] for col in desc], row))
            for row in cursor.fetchall()
        ]

    def query(self, sql, params: list):
        """
            手动查询sql
        :param sql:
        :param params:
        :return:
        """
        with self.conn.cursor() as cursor:
            cursor.execute(sql, params)
            result = self.dictfetchall(cursor)
            cursor.close()
            self.conn.close()
        return result


if __name__ == '__main__':
    import os

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "airport.settings")

    res = TKConnect('postgis').query(" select * from light ", [])
    print(res)
