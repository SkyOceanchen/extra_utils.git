import threading

import psycopg2
import json
from utils.map_utils.Map import TKMap
from django.db import transaction
from nkg.soilarea.models import CutGrassBase


class TKSoilAreaCal:

    def __init__(self, dbsettings):
        self.conn = psycopg2.connect(database=dbsettings['NAME'], user=dbsettings['USER'],
                                     password=dbsettings['PASSWORD'], host=dbsettings['HOST'], port=dbsettings['PORT'])
        self.layername = '土面区'

    def query(self):
        """
            土面区影响区geojson经过的土面区objectid列表
        :param sql:
        :param params:
        :return:
        """
        [query_field, tab] = TKMap().search_for_tab_queryfield(self.layername)
        sql = f"""SELECT {query_field}, st_astext(shape) as shape from {tab};"""
        cursor = self.conn.cursor()
        cursor.execute(sql)
        res = cursor.fetchall()
        return res

    def close(self):
        self.conn.commit()
        self.conn.close()
