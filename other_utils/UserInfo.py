import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", 'airport.settings')
import django

django.setup()
import hashlib
from utils.extra_utils.basic_type_operations.str_operation import StringOperation
from authority.models import User


class TkUserInfo():
    def __init__(self):
        self.User = User

    def change_user_psd(self):
        all_user = self.User.objects.filter(id__gt=1).order_by('id')
        for temp in all_user:
            new_psd = StringOperation.random_psd(8)
            print(new_psd)
            temp.set_password(hashlib.md5(bytes(new_psd, encoding='utf-8')).hexdigest())
            temp.save()

    # def import_student_data(self):


if __name__ == '__main__':
    TkUserInfo().change_user_psd()
