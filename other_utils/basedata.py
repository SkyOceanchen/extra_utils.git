import threading
import xlrd
from basic.models import *
import redis
from django.db import transaction
import psycopg2
from airport.settings import DATABASES
from string import digits
import re
from django.db.models import Q


class Postgres_Redis_Worker(object):
    conn = None
    pg_conn = None
    file_data = None
    cursor = None

    def __init__(self, database_info=DATABASES, host='120.55.53.65', port=16379, db=5):
        """
        默认链接数据库参数
        """
        self.database_info = database_info.get('postgis')
        self.host = host
        self.port = port
        self.db = db
        self.connect_redis(host=host, port=port, db=db)
        self.connect_postgres_host()

    def connect_redis(self, host='120.55.53.65', port=16379, db=5):
        """
        :param host: str
        :param port: int
        :param db: int
        :return:
        """
        pool = redis.ConnectionPool(host=host, port=port, db=db)  # 建立连接池
        self.conn = redis.Redis(connection_pool=pool)

    def connect_postgres_host(self):
        """创建连接并启用游标"""
        if not self.database_info:
            raise ConnectionError('项目数据库信息缺失，无法链接目标库')
        self.pg_conn = psycopg2.connect(database=self.database_info.get('NAME', ''),
                                        user=self.database_info.get('USER', ''),
                                        password=self.database_info.get('PASSWORD', ''),
                                        host=self.database_info.get('HOST', ''),
                                        port=self.database_info.get('PORT', ''))

        self.cursor = self.pg_conn.cursor()

    def execute_select_sql(self, sql=None, flat=None, msg=None):
        """
        查询数据库方法
        有flat时去除列表内元组，只有查询单字段sql可用,否则数据缺失，返回无元组列表
        pg 多次sql请求数据 如途中出错 无回退 会让pg事务抛异常 从而影响下次查询或修改
        """
        data = []
        # if not self.pg_conn:
        #     raise ConnectionError('未连接状态，是否未使用connect_host方法连接数据库并创建游标，请连接后重试')
        try:
            self.connect_postgres_host()
            if sql and flat and msg:
                self.cursor.execute(sql, msg)
                data = [i[0] for i in self.cursor.fetchall()]
            elif sql and flat:
                self.cursor.execute(sql)
                data = [i[0] for i in self.cursor.fetchall()]
            elif sql:
                data = self.cursor.fetchall
            self.conn.connection_pool.disconnect()
            return data
        except Exception as e:
            self.conn.connection_pool.disconnect()
            return []

    def close_connect(self):
        """关闭连接"""
        self.cursor = None
        self.conn.connection_pool.disconnect()
        self.conn = None


class Base_Data_Task_Tool(object):
    pg_worker = None
    redis_conn = None
    part_sheet = None
    area_sheet = None
    cell_sheet = None
    plate_sheet = None
    structureComposition_sheet = None
    airport_ID_sheet = None
    airport_seat_sheet = None
    runway_sheet = None
    part_sheet_nrows = 0
    area_sheet_nrows = 0
    cell_sheet_nrows = 0
    plate_sheet_nrows = 0
    airport_seat_sheet_nrows = 0
    runway_sheet_nrows = 0
    airport_ID_sheet_nrows = 0
    structureComposition_sheet_nrows = 0
    import_task_key = None

    def connect_database(self):
        """
        链接数据库方法
        :return:
        """
        self.pg_worker = Postgres_Redis_Worker()
        self.redis_conn = self.pg_worker.conn

    def close_connect(self):
        self.pg_worker.close_connect()

    def prepare_task(self):
        """
        开始任务前，必先调用此方法
        :return:
        """
        self.connect_database()
        file = self._args[0]
        self.import_task_key = self._args[1]
        workbook = xlrd.open_workbook(filename=None, file_contents=file)
        self.part_sheet = workbook.sheet_by_name('部位')
        self.area_sheet = workbook.sheet_by_name('区域')
        self.cell_sheet = workbook.sheet_by_name('单元')
        self.plate_sheet = workbook.sheet_by_name('板块')
        self.structureComposition_sheet = workbook.sheet_by_name('结构层')
        self.airport_ID_sheet = workbook.sheet_by_name('机场信息ID')
        self.airport_seat_sheet = workbook.sheet_by_name('机位信息')
        self.runway_sheet = workbook.sheet_by_name('跑道信息')
        self.part_sheet_nrows = self.part_sheet.nrows
        self.area_sheet_nrows = self.area_sheet.nrows
        self.cell_sheet_nrows = self.cell_sheet.nrows
        self.plate_sheet_nrows = self.plate_sheet.nrows
        self.airport_seat_sheet_nrows = self.airport_seat_sheet.nrows
        self.runway_sheet_nrows = self.runway_sheet.nrows
        self.airport_ID_sheet_nrows = self.airport_ID_sheet.nrows
        self.structureComposition_sheet_nrows = self.structureComposition_sheet.nrows

    @transaction.atomic()
    def import_struction_data(self):
        """
        导入结构层数据
        :return:
        """
        if self.structureComposition_sheet_nrows > 1:
            self.redis_conn.hmset(self.import_task_key, {'flag': '2', 'msg': '正在导入结构层数据'})
            create_list = []
            for i in range(1, self.structureComposition_sheet_nrows):
                title_col = self.structureComposition_sheet.ncols
                value = self.structureComposition_sheet.row_values(i)
                layers = title_col // 5
                structureCode = value[0]
                cluster_query = StructureCluster.objects.filter(structure_code=structureCode)
                cluster = cluster_query.first()
                structure_cluster = structureCode.translate(str.maketrans('', '', digits))
                if cluster:
                    cluster_query.update(structure_code=structureCode, structure_cluster=structure_cluster)
                else:
                    cluster = StructureCluster.objects.create(
                        structure_code=structureCode,
                        structure_cluster=structure_cluster
                    )
                cursor = 2
                for layer in range(0, layers):
                    defaults = {
                        'layer': layer + 1,
                        'material_name': value[cursor],
                        'thickness': float(value[cursor + 1]) if value[cursor + 1] else 0,
                        'elastic_modulus': value[cursor + 2],
                        'cbr': value[cursor + 3],
                        'poisson_ratio': float(value[cursor + 4]) if value[cursor + 4] else 0,
                        'structure_code': cluster
                    }
                    if not value[cursor]:  # 如发现没有结构层名称则跳过不创建
                        break
                    current_structure = Structure.objects.filter(structure_code=structureCode,
                                                                 layer=layer + 1)
                    if current_structure:
                        current_structure.update(**defaults)
                    else:
                        create_list.append(Structure(**defaults))
                    cursor += 5
                Structure.objects.bulk_create(create_list)
            print('结构层数据')
        return True

    @transaction.atomic()
    def import_part_data(self):
        """
        导入部位数据
        :return:
        """
        if self.part_sheet_nrows > 1:
            self.redis_conn.hmset(self.import_task_key, {'flag': '2', 'msg': '正在导入部位数据'})
            airport = Airport.objects.first()
            create_list = []
            for i in range(1, self.part_sheet_nrows):
                current_part_rows_info = self.part_sheet.row_values(i)
                partCode = current_part_rows_info[0]
                partName = current_part_rows_info[1]
                airCode = current_part_rows_info[2]
                partDis = current_part_rows_info[3]
                partLevel = current_part_rows_info[4]
                desc = current_part_rows_info[5]
                structCode = []
                if not partCode:
                    self.redis_conn.hmset(self.import_task_key, {'flag': '0', 'msg': '第%d行部位编号缺失,请确认' % (i + 1)})
                    return
                part_area = self.pg_worker.execute_select_sql(
                    sql="SELECT sum(st_area(shape)) from part where name =%(name)s",
                    flat=True,
                    msg={'name': str(partCode)}
                )
                if len(part_area) > 1:
                    if part_area[0]:
                        part_area = part_area[0]
                    else:
                        part_area = 0
                else:
                    part_area = 0
                defaults = {
                    'part_code': partCode,
                    'part_name': partName,
                    'airport_code': airCode,
                    'describe': partDis,
                    'remark': desc,
                    'airport': airport,
                    'level': partLevel,
                    'area_measure': round(part_area, 2),
                    'contain_structure': len(set(structCode)),
                    'contain_area': 0,
                }
                if not partName:
                    del defaults['part_name']
                if not airCode:
                    del defaults['airport_code']
                if not partDis:
                    del defaults['describe']
                if not partLevel:
                    del defaults['level']
                if not desc:
                    del defaults['remark']
                current_part = Part.objects.filter(part_code=partCode)
                if current_part:
                    current_part.update(**defaults)
                else:
                    create_list.append(Part(**defaults))
                print(i)
            Part.objects.bulk_create(create_list)
            print('部位')
        return True

    @transaction.atomic()
    def import_area_data(self):
        """
        导入区域数据
        :return:
        """
        if self.area_sheet_nrows > 1:
            create_list = []
            self.redis_conn.hmset(self.import_task_key, {'flag': '2', 'msg': '正在导入区域数据'})
            for i in range(1, self.area_sheet_nrows):
                currnet_rows_area_info = self.area_sheet.row_values(i)
                areaCode = currnet_rows_area_info[0]
                areaDis = currnet_rows_area_info[1]
                structCode = currnet_rows_area_info[2]
                ageOfConstruction = currnet_rows_area_info[3]
                desc = currnet_rows_area_info[4]
                part_code = self.area_sheet.row_values(i)[-1]
                if not areaCode:
                    self.redis_conn.hmset(self.import_task_key, {'flag': '0', 'msg': '第%d行区域编号缺失,请确认' % (i + 1)})
                    return
                if not part_code:
                    self.redis_conn.hmset(self.import_task_key, {'flag': '0', 'msg': '第%d行区域对应部位编号缺失,请确认' % (i + 1)})
                    return
                if not structCode:
                    self.redis_conn.hmset(self.import_task_key, {'flag': '0', 'msg': '第%d行区域对应结构缺失,请确认' % (i + 1)})
                    return
                if isinstance(ageOfConstruction, float):
                    ageOfConstruction = str(int(ageOfConstruction))
                area_area = self.pg_worker.execute_select_sql(
                    sql="SELECT sum(st_area(shape)) from area where name =%(name)s",
                    flat=True,
                    msg={'name': self.area_sheet.row_values(i)[0]}
                )
                this_part = Part.objects.filter(part_code=part_code).first()
                if not this_part:
                    self.redis_conn.hmset(self.import_task_key, {'flag': '0', 'msg': '%s该区域关联了无效的部位编号%s，第%d行'
                                                                                     % (areaCode, part_code, i + 1)})
                    return
                structure_cluster = StructureCluster.objects.filter(structure_code=structCode).first()
                if not structure_cluster:
                    self.redis_conn.hmset(self.import_task_key,
                                          {'flag': '0', 'msg': '第%d行区域对应结构无法对应数据库数据,请确认' % (i + 1)})
                    return
                if len(area_area) > 1:
                    if area_area[0]:
                        area_area = area_area[0]
                    else:
                        area_area = 0
                else:
                    area_area = 0
                defaults = {
                    'part': this_part,  # 对应部位
                    'area_code': areaCode,
                    'describe': areaDis,
                    'structure_cluster': structure_cluster,  # 对应结构
                    'construction': ageOfConstruction,
                    'remark': desc,
                    'area_measure': round(area_area, 2),
                    'contain_cell': 0,
                }
                if not areaDis:
                    del defaults['describe']
                if not ageOfConstruction:
                    del defaults['ageOfConstruction']
                if not desc:
                    del defaults['remark']

                current_area = Area.objects.filter(area_code=currnet_rows_area_info[0])
                if current_area:
                    current_area.update(**defaults)
                else:
                    create_list.append(Area(**defaults))
            Area.objects.bulk_create(create_list)
            print('区域')
        return True

    @transaction.atomic()
    def import_cell_data(self):
        """
        导入单元数据
        :return:
        """
        # 单元部分

        if self.cell_sheet_nrows > 1:
            create_list = []
            self.redis_conn.hmset(self.import_task_key, {'flag': '2', 'msg': '正在导入单元数据(该部分数据量大，请耐心等待)'})
            for i in range(1, self.cell_sheet_nrows):
                current_rows_cell_info = self.cell_sheet.row_values(i)
                cell_code = current_rows_cell_info[0]
                area_code = current_rows_cell_info[-1]
                if not cell_code:
                    self.redis_conn.hmset(self.import_task_key, {'flag': '0', 'msg': ' 第%d行单元编号缺失,请确认' % (i + 1)})
                    return
                if not area_code:
                    self.redis_conn.hmset(self.import_task_key, {'flag': '0', 'msg': '第%d行对应区域编号缺失,请确认' % (i + 1)})
                    return
                cell_area = self.pg_worker.execute_select_sql(
                    sql="SELECT sum(st_area(shape)) from cell where name =%(name)s",
                    flat=True,
                    msg={'name': cell_code}
                )
                this_area = Area.objects.filter(area_code=area_code).first()
                if not this_area:
                    self.redis_conn.hmset(self.import_task_key, {'flag': '0', 'msg': '%s该单元关联了无效的区域编号%s,第%d行'
                                                                                     % (cell_code, area_code, i + 1)})
                    return
                current_cell = Cell.objects.filter(cell_code=cell_code)
                if len(cell_area) > 1:
                    if cell_area[0]:
                        cell_area = cell_area[0]
                    else:
                        cell_area = 0
                else:
                    cell_area = 0
                defaults = {
                    'area': this_area,
                    'cell_code': cell_code,
                    'area_measure': round(cell_area, 2),
                    'contain_plate': 0,
                }
                if current_cell:
                    Cell.objects.filter(cell_code=cell_code).update(**defaults)
                else:
                    create_list.append(Cell(**defaults))
            Cell.objects.bulk_create(create_list)
            print('单元')
        return True

    @transaction.atomic()
    def import_plate_data(self):
        """
        导入板块部分
        :return:
        """
        # 板块部分
        if self.plate_sheet_nrows > 1:
            self.redis_conn.hmset(self.import_task_key, {'flag': '2', 'msg': '正在导入板块数据(该部分数据量大，请耐心等待)'})
            create_list = []
            for i in range(1, self.plate_sheet_nrows):
                current_rows_plate_info = self.plate_sheet.row_values(i)
                rowNo = current_rows_plate_info[0]
                colNo = current_rows_plate_info[1]
                platelength = current_rows_plate_info[2]
                shape = current_rows_plate_info[3]
                cell_code = current_rows_plate_info[-1]
                if not rowNo:
                    self.redis_conn.hmset(self.import_task_key, {'flag': '0', 'msg': '第%d行行编号编号缺失,请确认' % (i + 1)})
                    return
                if not colNo:
                    self.redis_conn.hmset(self.import_task_key, {'flag': '0', 'msg': '第%d行列编号缺失,请确认' % (i + 1)})
                    return
                if not cell_code:
                    self.redis_conn.hmset(self.import_task_key, {'flag': '0', 'msg': '第%d行对应单元编号缺失,请确认' % (i + 1)})
                    return
                this_cell = Cell.objects.filter(cell_code=cell_code).first()
                if not this_cell:
                    self.redis_conn.hmset \
                        (self.import_task_key, {'flag': '0', 'msg': '%s%s该板块关联了无效的单元编号%s,第%d行'
                                                                    % (rowNo, colNo, cell_code, i + 1)})
                    return
                plate_area = self.pg_worker.execute_select_sql(
                    sql="SELECT sum(st_area(shape)) from plate where name =%(name)s and unitname =%(unitname)s",
                    flat=True,
                    msg={'name': rowNo + ' ' + colNo,
                         'unitname': this_cell.cellCode}
                )
                defaults = {
                    'row': rowNo,
                    'col': colNo,
                    'length': platelength,
                    'shape': shape,
                    'cell': this_cell,
                    'area_measure': str(plate_area[0])[0:10] if plate_area else '',
                }
                if not platelength:
                    del defaults['length']
                if not shape:
                    del defaults['shape']
                current_plate = Plate.objects.filter(row=rowNo, col=colNo, cell=this_cell)
                if current_plate:
                    current_plate.update(**defaults)
                else:
                    create_list.append(Plate(**defaults))
            Plate.objects.bulk_create(create_list)
            print('板块')
        return True

    @transaction.atomic()
    def import_stand_data(self):
        """
        导入机位数据
        :return:
        """
        # 机位信息部分
        if self.airport_seat_sheet_nrows > 1:
            self.redis_conn.hmset(self.import_task_key, {'flag': '2', 'msg': '正在导入机位数据'})
            create_list = []
            for i in range(1, self.airport_seat_sheet_nrows):
                current_rows_seat_info = self.airport_seat_sheet.row_values(i)
                size_of_parkStand = current_rows_seat_info[0]
                pcn = current_rows_seat_info[1]
                width_of_wingspan = current_rows_seat_info[2]
                parkable = current_rows_seat_info[3]
                parkStandCode = current_rows_seat_info[4]
                usage = current_rows_seat_info[5]
                type_of_mark = current_rows_seat_info[6]
                area_of_mark = current_rows_seat_info[7]
                type_of_parkStand = current_rows_seat_info[8]
                part_code = current_rows_seat_info[-1]
                if isinstance(parkStandCode, float):
                    parkStandCode = str(int(parkStandCode))
                if not parkStandCode:
                    self.redis_conn.hmset(self.import_task_key, {'flag': '0', 'msg': '第%d行机位号缺失' % (i + 1)})
                    return
                if not part_code:
                    self.redis_conn.hmset(self.import_task_key, {'flag': '0', 'msg': '第%d行所属部位编号缺失,请确认' % (i + 1)})
                    return
                part = Part.objects.filter(part_code=part_code).first()
                if not part:
                    self.redis_conn.hmset(self.import_task_key, {'flag': '0', 'msg': '%s该机位关联了无效的部位编号%s, 第%d行'
                                                                                     % (
                                                                                         parkStandCode, part_code,
                                                                                         i + 1)})
                    return
                defaults = {
                    'size_of_parkStand': size_of_parkStand,
                    'pcn': pcn,
                    'width_of_wingspan': width_of_wingspan if not width_of_wingspan else 0,
                    'parkable': parkable,
                    'stand_code': parkStandCode,
                    'usage': usage,
                    'type_of_mark': type_of_mark if type_of_mark else 0,
                    'area_of_mark': area_of_mark if area_of_mark else 0,
                    'type_of_stand': type_of_parkStand,
                    'part': part,
                }
                if not size_of_parkStand:
                    del defaults['size_of_parkStand']
                if not pcn:
                    del defaults['pcn']
                if not parkable:
                    del defaults['parkable']
                if not usage:
                    del defaults['usage']
                if not type_of_parkStand:
                    del defaults['type_of_stand']
                current_seat = Stand.objects.filter(stand_code=parkStandCode)
                if current_seat:
                    current_seat.update(**defaults)
                else:
                    create_list.append(Stand(**defaults))
            Stand.objects.bulk_create(create_list)
            print('机位')
        return True

    @transaction.atomic()
    def import_runway_data(self):
        """
        导入跑道数据
        :return:
        """
        # 跑道信息部分
        if self.runway_sheet_nrows > 1:
            self.redis_conn.hmset(self.import_task_key, {'flag': '2', 'msg': '正在导入跑道数据'})
            create_list = []
            airport = Airport.objects.first()
            if not airport:
                self.redis_conn.hmset(self.import_task_key, {'flag': '0', 'msg': '请确认机场信息'})
                return
            for i in range(1, self.runway_sheet_nrows):
                current_rows_runway_info = self.runway_sheet.row_values(i)
                runway_code = current_rows_runway_info[0]
                runwayName = current_rows_runway_info[1]
                length_of_runway = current_rows_runway_info[3]
                width_of_runway = current_rows_runway_info[4]
                operation_category = current_rows_runway_info[5]
                base_thickness = current_rows_runway_info[6]
                runway_level = current_rows_runway_info[7]
                building_time = current_rows_runway_info[8]
                sizeOfLiftingBelt = current_rows_runway_info[9]
                sizeOfBlastPad = current_rows_runway_info[10]
                widthOfShoulder = current_rows_runway_info[11]
                sizeOfSafetyZone = current_rows_runway_info[12]
                takeoffDistance = current_rows_runway_info[13]
                takeoffRunDistance = current_rows_runway_info[14]
                accelerateStopDistance = current_rows_runway_info[15]
                landingDistance = current_rows_runway_info[16]
                runwayFrictionCoefficient = current_rows_runway_info[17]
                crossSlope = current_rows_runway_info[18]
                effectiveLongitudinalSlope = current_rows_runway_info[19]
                bulletinPCN = current_rows_runway_info[20]
                shippingTime = current_rows_runway_info[21]
                runway_area = 0
                if length_of_runway and width_of_runway:
                    runway_area = round(length_of_runway, 0) * round(width_of_runway, 0)
                if not runway_code:
                    self.redis_conn.hmset(self.import_task_key, {'flag': '0', 'msg': '第%d行跑道编号缺失,请确认' % (i + 1)})
                    return
                if isinstance(takeoffRunDistance, float):
                    takeoffRunDistance = str(int(takeoffRunDistance))
                defaults = {'runway_name': runwayName,
                            'airport': airport,
                            'runway_code': runway_code,
                            'size_of_runway': str(int(width_of_runway)) + '*' + str(int(length_of_runway)),  # 跑道尺寸
                            'operation_category': operation_category,
                            'base_thickness': base_thickness,  # 基层厚度
                            'level': runway_level,
                            'building_time': building_time,  # 建筑时间
                            'size_of_lifting_belt': sizeOfLiftingBelt,
                            'size_of_blast_pad': sizeOfBlastPad,
                            'width_of_shoulder': widthOfShoulder,
                            'size_of_safety_zone': sizeOfSafetyZone,
                            'takeoff_distance': takeoffDistance,
                            'takeoff_run_distance': takeoffRunDistance,
                            'accelerate_stop_distance': accelerateStopDistance,
                            'landing_distance': landingDistance,
                            'friction': runwayFrictionCoefficient,
                            'cross_slope': crossSlope,
                            'longitudinal_slope': effectiveLongitudinalSlope,
                            'pcn': bulletinPCN,
                            'shipping_time': shippingTime,  # 通航时间
                            }
                if not runwayName:
                    del defaults['runway_name']
                if not operation_category:
                    del defaults['operation_category']
                if not base_thickness:
                    del defaults['base_thickness']
                if not runway_level:
                    del defaults['level']
                if not building_time:
                    del defaults['building_time']
                if not sizeOfLiftingBelt:
                    del defaults['size_of_lifting_belt']
                if not sizeOfBlastPad:
                    del defaults['size_of_blast_pad']
                if not widthOfShoulder:
                    del defaults['width_of_shoulder']
                if not sizeOfSafetyZone:
                    del defaults['size_of_safety_zone']
                if not takeoffDistance:
                    del defaults['takeoff_distance']
                if not takeoffRunDistance:
                    del defaults['takeoff_run_distance']
                if not accelerateStopDistance:
                    del defaults['accelerate_stop_distance']
                if not landingDistance:
                    del defaults['landing_distance']
                if not runwayFrictionCoefficient:
                    del defaults['friction']
                if not crossSlope:
                    del defaults['cross_slope']
                if not effectiveLongitudinalSlope:
                    del defaults['longitudinal_slope']
                if not bulletinPCN:
                    del defaults['pcn']
                if not shippingTime:
                    del defaults['shipping_time']
                current_runway = Runway.objects.filter(runway_code=runway_code)
                if current_runway:
                    current_runway.update(**defaults)
                else:
                    create_list.append(Runway(**defaults))
            Runway.objects.bulk_create(create_list)
            print('跑道')
        return True

    @transaction.atomic()
    def import_airport_data(self):
        # 机场部分
        if self.airport_ID_sheet_nrows > 1:
            self.redis_conn.hmset(self.import_task_key, {'flag': '2', 'msg': '正在导入机场数据'})
            current_airport_info = self.airport_ID_sheet.row_values(1)
            smoothroad_number = Part.objects.filter(describe='滑行道').count()
            parkapron_number = Part.objects.filter(describe__contains='机坪').count()
            runway_number = Runway.objects.all().count()
            park_number = Stand.objects.all().count()
            airport_name = current_airport_info[0]
            city = current_airport_info[1]
            air_gis_url = current_airport_info[2]
            air_gis_tile_url = current_airport_info[3]
            air_center_point = current_airport_info[4]
            manage_institution = current_airport_info[5]
            affiliate = current_airport_info[6]
            airport_level = current_airport_info[7]
            benchmark_height = current_airport_info[8]
            reference_system = current_airport_info[9]
            shipping_time = current_airport_info[10]
            remark = current_airport_info[11]
            airport_code = current_airport_info[12]
            covered = str(current_airport_info[13])
            quickroad_number = int(current_airport_info[14]) if current_airport_info[14] else 0
            liaisonroad_number = int(current_airport_info[15]) if current_airport_info[15] else 0
            asphalt_area = 0
            cement_area = 0
            runway_area = 0
            apron_area = 0
            drainage_ditch_length = 0
            boundary_length = self.pg_worker.execute_select_sql(sql="SELECT sum(st_length(shape)) from enclosure",
                                                                flat=True)
            roadsurface_area = self.pg_worker.execute_select_sql(sql="select sum(st_area(shape)) from part", flat=True)
            AC_code_list = []
            struction_list = Area.objects.distinct().values('area_code', 'structure_cluster_id')
            for i in struction_list:
                if i['structure_cluster_id'].startswith('AC') or i['structure_cluster_id'].startswith('PAC'):
                    AC_code_list.append(i['area_code'])
            for AC_code in AC_code_list:
                query_data = self.pg_worker.execute_select_sql(
                    sql='select sum(st_area(shape)) from area where name = %(AC_code)s',
                    flat=True,
                    msg={'AC_code': AC_code})
                if len(query_data) >= 1:
                    asphalt_area += query_data[0] if query_data[0] else 0
            PC_code_list = []
            for i in struction_list:
                if i['structure_cluster_id'].startswith('PCC'):
                    PC_code_list.append(i['area_code'])
            for PC_code in PC_code_list:
                query_data = self.pg_worker.execute_select_sql(
                    sql='select sum(st_area(shape)) from area where name = %(PC_code)s',
                    flat=True,
                    msg={'PC_code': PC_code})
                if len(query_data) >= 1:
                    cement_area += query_data[0] if query_data[0] else 0
            all_tables = self.pg_worker.execute_select_sql(sql="select tablename from pg_tables", flat=True)

            part_count = 0
            area_count = 0
            cell_count = 0
            signboard_oper_count = 0
            light_count = 0
            service_lane_length = 0
            soilsurface_area = 0

            for table in all_tables:
                if table.startswith('soil'):
                    sql = "select sum(st_area(shape)) from " + table
                    soilsurface_area_list = self.pg_worker.execute_select_sql(sql=sql, flat=True)
                    if len(soilsurface_area_list) > 1:
                        soilsurface_area += soilsurface_area_list[0] if soilsurface_area_list[0] else 0
                if table == 'part':
                    sql = "select count(*) from " + table
                    part_count_list = self.pg_worker.execute_select_sql(sql=sql, flat=True)
                    if len(part_count_list) > 1:
                        part_count = part_count_list[0] if part_count_list[0] else 0
                if table == 'area':
                    sql = "select count(*) from " + table
                    area_count_list = self.pg_worker.execute_select_sql(sql=sql, flat=True)
                    if len(area_count_list) > 1:
                        area_count = area_count_list[0] if area_count_list[0] else 0
                if table == 'cell':
                    sql = "select count(*) from " + table
                    cell_count_list = self.pg_worker.execute_select_sql(sql=sql, flat=True)
                    if len(cell_count_list) > 1:
                        cell_count = cell_count_list[0] if cell_count_list[0] else 0
                if table == 'signboard_oper':
                    sql = "select count(*) from " + table
                    signboard_oper_count_list = self.pg_worker.execute_select_sql(sql=sql, flat=True)
                    if len(signboard_oper_count_list) > 1:
                        signboard_oper_count = signboard_oper_count_list[0] if signboard_oper_count_list else 0
                if table == 'light':
                    sql = "select count(*) from " + table
                    light_count_list = self.pg_worker.execute_select_sql(sql=sql, flat=True)
                    if len(light_count_list) > 1:
                        light_count = light_count_list[0] if light_count_list[0] else 0
                if table == 'median':
                    sql = "select sum(st_length(shape)) from " + table
                    median_length_list = self.pg_worker.execute_select_sql(sql=sql, flat=True)
                    if len(median_length_list) > 1:
                        service_lane_length = median_length_list[0] if median_length_list[0] else 0
            runway_code_list = Part.objects.filter(describe='跑道').values_list('part_code', flat=True)
            for runway_code in runway_code_list:
                query_data = self.pg_worker.execute_select_sql(
                    sql="select sum(st_area(shape)) from part where name = %(runway_code)s",
                    flat=True,
                    msg={'runway_code': runway_code})
                if len(query_data) >= 1:
                    runway_area += query_data[0] if query_data[0] else 0
            apron_code_list = Part.objects.filter(describe__contains='机坪').values_list('part_code', flat=True)
            for apron_code in apron_code_list:
                query_data = self.pg_worker.execute_select_sql(
                    sql="select sum(st_area(shape)) from part where name = %(apron_code)s",
                    flat=True,
                    msg={'apron_code': apron_code})
                if len(query_data) >= 1:
                    apron_area += query_data[0] if query_data[0] else 0
            covered_float = re.search('[0-9]+.[0-9]+', covered)
            if len(roadsurface_area) > 1:
                if roadsurface_area[0]:
                    roadsurface_area = roadsurface_area[0]
                else:
                    roadsurface_area = 0
            else:
                roadsurface_area = 0
            if len(boundary_length) > 1:
                if boundary_length[0]:
                    boundary_length = boundary_length[0]
                else:
                    boundary_length = 0
            else:
                boundary_length = 0
            defaults = {
                'airport_name': airport_name,  # c
                'city': city,  # c
                'air_gis_url': air_gis_url,  # c
                'air_gis_tile_url': air_gis_tile_url,  # c
                'air_center_point': air_center_point,  # 机场中心点坐标
                'manage_institution': manage_institution,  # c
                'affiliate': affiliate,  # c
                'airport_level': airport_level,  # c
                'benchmark_height': benchmark_height,  # f
                'reference_system': reference_system,  # c
                'shipping_time': shipping_time,  # c
                'remark': remark,  # c
                'iata': airport_code,  # 机场代码
                'runway_number': runway_number,  # i
                'smoothroad_number': smoothroad_number,  # i
                'liaisonroad_number': liaisonroad_number,  # i
                'quickroad_number': quickroad_number,  # i
                'park_number': park_number,  # i
                'parkapron_number': parkapron_number,  # i
                'roadsurface_area': round(roadsurface_area, 2),
                'asphalt_area': round(asphalt_area, 2),
                'cement_area': round(cement_area, 2) if cement_area else 0,
                'soilsurface_area': round(soilsurface_area, 2),
                'boundary_length': round(boundary_length, 2),
                'covered': covered if isinstance(covered, float) else covered_float.group(),  # TODO 123.3公顷
                'runway_area': round(runway_area, 2),  # f
                'apron_area': round(apron_area, 2),  # f
                'service_lane_length': round(service_lane_length, 2),
                'drainage_ditch_length': drainage_ditch_length,
                'part_count': part_count,  # 部位数量
                'area_count': area_count,  # 区域数量
                'cell_count': cell_count,  # 单元数量
                'signboard_count': signboard_oper_count,  # 标志牌数量
                'light_count': light_count,  # 灯光数量
            }
            if not airport_name:
                del defaults['airport_name']
            if not city:
                del defaults['city']
            if not air_gis_url:
                del defaults['air_gis_url']
            if not air_gis_tile_url:
                del defaults['air_gis_tile_url']
            if not air_center_point:
                del defaults['air_center_point']
            if not manage_institution:
                del defaults['manage_institution']
            if not affiliate:
                del defaults['affiliate']
            if not airport_level:
                del defaults['airport_level']
            if not benchmark_height:
                del defaults['benchmark_height']
            if not reference_system:
                del defaults['reference_system']
            if not shipping_time:
                del defaults['shipping_time']
            if not remark:
                del defaults['remark']
            if not airport_code:
                del defaults['iata']
            if not covered:
                del defaults['covered']
            current_airport = Airport.objects.filter(id=1)
            if current_airport:
                current_airport.update(**defaults)
            else:
                Airport.objects.create(**defaults)
            print('机场')
        return True

    @transaction.atomic()
    def completion_data(self):
        """
        补全数据
        :return:
        """
        # 补充对应 部位/区域/单元对应数量
        self.redis_conn.hmset(self.import_task_key, {'flag': '2', 'msg': '正在补充部位/区域/单元对应数量'})
        if self.part_sheet_nrows > 1:
            for i in range(1, self.part_sheet.nrows):
                partCode = self.part_sheet.row_values(i)[0]
                if isinstance(partCode, float):
                    partCode = str(int(partCode))
                part_query = Part.objects.filter(part_code=partCode)
                part = part_query.first()
                area_query = Area.objects.filter(part=part)
                if not part:
                    self.redis_conn.hmset(self.import_task_key, {'flag': '0', 'msg': '未能找到%s该部位，无法更新对应区域数量' % partCode})
                    return
                code_list = area_query.values_list('structure_cluster_id', flat=True)
                code_list = StructureCluster.objects.filter(structure_code__in=code_list).values_list('structure_code',
                                                                                                      flat=True)
                containsStructureNum = len(set(code_list))
                containsAreaNum = area_query.count()
                part_query.update(contain_area=containsAreaNum, contain_structure=containsStructureNum)

        if self.area_sheet_nrows > 1:
            for i in range(1, self.area_sheet_nrows):
                areaCode = self.area_sheet.row_values(i)[0]
                area_query = Area.objects.filter(area_code=areaCode)
                area = area_query.first()
                if not area:
                    self.redis_conn.hmset(self.import_task_key, {'flag': '0', 'msg': '未能找到%s该区域，无法更新对应单元数量' % areaCode})
                    return
                containsCellNum = Cell.objects.filter(area=area).count()
                area_query.update(contain_cell=containsCellNum)

        if self.cell_sheet_nrows > 1:
            for i in range(1, self.cell_sheet_nrows):
                cellCode = self.cell_sheet.row_values(i)[0]
                cell_query = Cell.objects.filter(cell_code=cellCode)
                cell = cell_query.first()
                if not cell:
                    self.redis_conn.hmset(self.import_task_key, {'flag': '0', 'msg': '未能找到%s该单元，无法更新对应板块数量' % areaCode})
                    return
                containsPlateNum = Plate.objects.filter(cell=cell).count()
                cell_query.update(contain_plate=containsPlateNum)
        print('补全')
        return True

    def start_task(self):
        """
        运行此次任务的方法 ，如单独想录入部分数据可在此修改,但调用顺序无法改变
        注意：
        1、机场表 需要有一条数据
        2、由于数据存在关联性，需要按此顺序调用方法且确保执行完毕之后才能执行下一个
        3、completion_data方法是补充各个板块对应数量和关联信息的方法、建议每次都调用
        4、
        :return:
        """
        self.prepare_task()  # 任务前必须调用
        if self.import_struction_data():
            if self.import_part_data():  #
                if self.import_area_data():
                    if self.import_cell_data():
                        if self.import_plate_data():
                            if self.import_stand_data():
                                if self.import_runway_data():
                                    if self.import_airport_data():
                                        if self.completion_data():
                                            self.redis_conn.hmset(self.import_task_key, {'flag': '3', 'msg': '导入数据完成'})
                                            self.pg_worker.close_connect()


class Update_Base_Data_Thread(threading.Thread, Base_Data_Task_Tool):

    def run(self):
        try:
            self.start_task()
        except Exception as e:
            self.connect_database()
            self.redis_conn.hmset(self.import_task_key, {'flag': '0', 'msg': str(e)})
            self.close_connect()


class Base_Data_Worker(Postgres_Redis_Worker, Update_Base_Data_Thread):

    def get_task_status(self, import_key):
        """
        注意：如果任务中途非正常中断、需要手动将redis字段flag置空
        :return: flag ,msg, msg1(是否第一次任务)
        flag:'' 任务未开始
        flag:2 任务中的状态
        flag:3 已经完成
        flag:0 任务中出错
        flag1: 为真则是第一次运行
        msg: 任务信息
        """
        flag = self.conn.hget(import_key, 'flag')
        msg = self.conn.hget(import_key, 'msg')
        if not msg and not flag:
            msg = '未开启任务'
            return flag, msg, True
        if flag.decode('utf8') == '0' or flag.decode('utf8') == '3':
            self.conn.hmset(import_key, {'flag': '', 'msg': '任务等待开始'})
        return flag, msg, None

    def start_task(self, import_key, file):
        """
        （注意：xlrd==>1.2.1 , redis==>3.3.6 版本必须）
        需要传入 import_key 此项目任务唯一标记，作为redis_hash的键 启动任务
        仅支持后台只有一个导入任务，
        开始任务后需要调用 get_task_status（）方法获取任务状态
        :return:  msg :信息数据
        """
        self.file_data = file
        if not self.file_data:
            return '请上传文件数据'
        import_task_key = import_key
        if not import_task_key:
            return '请传入一个任务唯一标识键'
        flag, msg, flag1 = self.get_task_status(import_key=import_task_key)
        if flag1:
            self.conn.hmset(import_key, {'flag': '', 'msg': '任务等待开始'})
        if flag and msg:
            return '当前已经在进行资料导入，请稍等再重试'
        theading = Update_Base_Data_Thread(args=(file.read(), import_key))  # 开启分支线程进行导入
        theading.start()
