import requests


class Weather:
    appid = '43398825'
    appsecret = '4HYOq6fg'
    appcode = '1e7b73b7951f4115926499e7e419d3f6'
    host = 'https://weather01.market.alicloudapi.com'
    path = '/ip-to-weather'

    @staticmethod
    def get_weather():
        try:
            r = requests.get(
                f"https://tianqiapi.com/free/day?&unescape=1&appid={Weather.appid}&appsecret={Weather.appsecret}")
            r.encoding = 'utf-8'
            weather = r.json()
        except Exception as e:
            weather = dict()
        return weather

    def get_aliweather(self, ip):
        # http://ip.yqie.com/cn/shanghai/
        # if ip == '127.0.0.1':
        #     ip = '58.247.44.170' #上海
        querys = f'''ip={ip}&need3HourForcast=0&needAlarm=0&needHourData=0&needIndex=0&needMoreDay=0'''
        url = self.host + self.path + '?' + querys
        r = requests.get(url, headers={'Authorization': 'APPCODE ' + self.appcode})
        if r.status_code == 555:
            # 错误
            return None
        else:
            r.encoding = 'utf-8'
            weather = r.json()
            return weather['showapi_res_body']


def get_weather_from_wthrcdn(city):
    try:
        r = requests.get("http://wthrcdn.etouch.cn/weather_mini?city=" + city)
        r.encoding = 'utf-8'
        wendu = r.json()['data']['wendu']
        weather = r.json()['data']['forecast'][0]['type']
    except Exception as e:
        wendu = ''
        weather = ''
    return wendu, weather


if __name__ == '__main__':
    pass
    # res = Weather.get_weather()
    # print(res)
    # w = Weather()
    # 58.247.44.170  58.38.164.255
